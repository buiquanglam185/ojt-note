packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "nginx-ubuntu-thanhhv18"
  instance_type = "t2.micro"
  region        = "ca-central-1"
  vpc_filter {
    filters = {
      "tag:Name" = "thanhhv18-ojt-vpc"
    }
  }
  subnet_filter {
    filters = {
      "tag:Name" = "thanhhv18-ojt-public-subnet-*"
    }
    random = true
  }
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  ssh_username = "ubuntu"
}

build {
  name = "learn-packer"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]
  provisioner "shell" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y nginx",
      "echo 'Hello, TOM!' | sudo tee /var/www/html/index.html",
    ]
  }
}