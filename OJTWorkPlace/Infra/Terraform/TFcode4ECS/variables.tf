variable "region" {
  default = "ca-central-1"
  type = string
}
variable "profile" {
  default = "default"
  type = string
}
variable "vpc_name" {
  default = "thanhhv18-ojt-vpc"
  type = string
}
variable "environment" {
  default = "dev"
  type = string
}
variable "vpc_main_cidr" {
  type = string
  default = "10.0.0.0/16"
}
variable "map_public_ip_on_launch" {
  type = bool
  default = true
}
variable "public_subnet_cidr_1" {
  type = string
  default = "10.0.1.0/24"
}
variable "public_subnet_cidr_2" {
  type = string
  default = "10.0.2.0/24"
}
variable "availability_zone_1" {
  type = string
  default = "ca-central-1a"
}
variable "availability_zone_2" {
  type = string
  default = "ca-central-1b"
}
variable "igw_name" {
  type = string
  default = "thanhhv18-ojt-igw"
}
variable "route_table_name" {
  default = "thanhhv18-ojt-route-table"
  type = string
}
variable "public_subnet_1_name" {
  default = "thanhhv18-ojt-public-subnet-1"
  type = string
}
variable "public_subnet_2_name" {
  default = "thanhhv18-ojt-public-subnet-2"
  type = string
}
variable "security_group_name" {
  default = "thanhhv18-ojt-security-group"
  type = string
}

variable "ecr_name" {
  default = "thanhhv18-ojt-ecr"
  type = string
}
variable "ecs_cluster_name" {
  default = "thanhhv18-ojt-ecs-Cluster"
  type = string
}
variable "ecs_task_definition_name" {
  default = "thanhhv18-ojt-ecs-task-definition"
  type = string
}
variable "ecs_task_execution_role_name" {
  default = "thanhhv18-ojt-ecs-task-execution-role"
  type = string
}
variable "ecs_service_name" {
  default = "thanhhv18-ojt-ecs-Service"
  type = string
}
variable "lb_target_group_name_1" {
  default = "thanhhv18-ojt-lb-target-group-1"
  type = string
}
variable "lb_target_group_name_2" {
  default = "thanhhv18-ojt-lb-target-group-2"
  type = string
}
variable "lb_name" {
  default = "thanhhv18-ojt-lb"
  type = string
}
variable "target_value" {
    type = number
    default = 50
}
variable "max_capacity" {
    type = number
    default = 4
}
variable "min_capacity" {
    type = number
    default = 2
}
variable "ecs_task_execution_role_policy_name" {
    type = string
    default = "thanhhv18-ojt-ecs-task-execution-role-policy"
}
variable "nginx_image_name" {
    type = string
    default = "thanhhv18-ojt-ecr:latest"
}
variable "account_id" {
    type = string
    default = "541253215789"
}
variable "ecs_task_execution_role_policy_cloudwatch_name" {
    type = string
    default = "thanhhv18-ojt-ecs-task-execution-role-policy-cloudwatch"
}
variable "codedeploy_app_name" {
    type = string
    default = "thanhhv18-ojt-codedeploy-app"
}
variable "codedeploy_compute_platform" {
    type = string
    default = "ECS"
}
variable "deployment_group_name" {
    type = string
    default = "thanhhv18-ojt-codedeploy-deployment-group"
}
variable "codedeploy_auto_rollback_enabled" {
    type = bool
    default = true
}
variable "codedeploy_auto_rollback_events" {
    type = list(string)
    default = ["DEPLOYMENT_FAILURE"]
}
variable "codedeploy_deployment_ready_option_action_on_timeout" {
    type = string
    default = "STOP_DEPLOYMENT"
}
variable "codedeploy_deployment_ready_option_wait_time_in_minutes" {
    type = number
    default = 1
}
variable "codedeploy_terminate_blue_instances_on_deployment_success_termination_wait_time_in_minutes" {
    type = number
    default = 1
}
variable "codedeploy_IAM_role_name" {
    type = string
    default = "thanhhv18-ojt-codedeploy-IAM-role"
}
variable "iam_path" {
    type = string
    default = "/service-role/"
}