module "network" {
    source = "./modules/network"
    vpc_name = var.vpc_name
    vpc_cidr = var.vpc_main_cidr
    vpc_main_cidr = var.vpc_main_cidr
    environment = var.environment
    map_public_ip_on_launch = var.map_public_ip_on_launch
    public_subnet_cidr_1 = var.public_subnet_cidr_1
    public_subnet_cidr_2 = var.public_subnet_cidr_2
    availability_zone_1 = var.availability_zone_1
    availability_zone_2 = var.availability_zone_2
    public_subnet_1_name = var.public_subnet_1_name
    public_subnet_2_name = var.public_subnet_2_name
    igw_name = var.igw_name
    route_table_name = var.route_table_name
    security_group_name = var.security_group_name
}

module "ecs" {
    source = "./modules/ecs"
    ecr_name = var.ecr_name
    ecs_cluster_name = var.ecs_cluster_name
    environment = var.environment
    ecs_task_definition_name = var.ecs_task_definition_name
    region = var.region
    ecs_task_execution_role_name = var.ecs_task_execution_role_name
    ecs_service_name = var.ecs_service_name
    lb_target_group_name_1 = var.lb_target_group_name_1
    lb_target_group_name_2 = var.lb_target_group_name_2
    security_group_id = module.network.security_group_id
    public_subnet_1_id = module.network.public_subnet_1_id
    public_subnet_2_id = module.network.public_subnet_2_id
    lb_name = var.lb_name
    ecs_task_execution_role_policy_name = var.ecs_task_execution_role_policy_name
    vpc_id = module.network.vpc_id
    nginx_image_name = var.nginx_image_name
    account_id = var.account_id
    ecs_task_execution_role_policy_cloudwatch_name = var.ecs_task_execution_role_policy_cloudwatch_name
    codedeploy_app_name = var.codedeploy_app_name
    codedeploy_compute_platform = var.codedeploy_compute_platform
    deployment_group_name = var.deployment_group_name
    codedeploy_auto_rollback_enabled = var.codedeploy_auto_rollback_enabled
    codedeploy_auto_rollback_events = var.codedeploy_auto_rollback_events
    codedeploy_deployment_ready_option_action_on_timeout = var.codedeploy_deployment_ready_option_action_on_timeout
    codedeploy_deployment_ready_option_wait_time_in_minutes = var.codedeploy_deployment_ready_option_wait_time_in_minutes
    codedeploy_terminate_blue_instances_on_deployment_success_termination_wait_time_in_minutes = var.codedeploy_terminate_blue_instances_on_deployment_success_termination_wait_time_in_minutes
    codedeploy_IAM_role_name = var.codedeploy_IAM_role_name
    iam_path = var.iam_path
    
    depends_on = [module.network]
}

module "autoscale" {
    source = "./modules/autoscale"
    max_capacity = var.max_capacity
    min_capacity = var.min_capacity
    ecs_cluster_name = module.ecs.ecs_cluster_name
    ecs_service_name = module.ecs.ecs_service_name
    target_value = var.target_value
    depends_on = [module.ecs]
}