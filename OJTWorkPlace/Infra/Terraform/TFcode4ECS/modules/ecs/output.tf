output "ecs_cluster_name" {
  value =  aws_ecs_cluster.ecs_cluster.name
}
output "ecs_service_name" {
  value = aws_ecs_service.ecs_service.name
}
output "ecs_task_definition_name" {
  value = aws_ecs_task_definition.ecs_task_definition.family
}
output "ecs_task_execution_role_name" {
  value = aws_iam_role.ecs_task_execution_role.name
}
output "ecs_task_execution_role_policy_name" {
  value = aws_iam_role_policy.ecs_task_execution_role_policy.name
}
output "lb_name" {
  value = aws_lb.lb.name
}
output "lb_target_group_name_1" {
  value = aws_lb_target_group.lb_target_group_1.name
}
output "lb_target_group_name_2" {
  value = aws_lb_target_group.lb_target_group_2.name
}
output "ecr_name" {
  value = aws_ecr_repository.ecr.name
}