package test

//
// Language: go
// Path: OJT WorkPlace/Infra/Terraform/SimpleAWS/test/terraform_aws_network_test.go
//
import (
	//"crypto/tls"
	"github.com/gruntwork-io/terratest/modules/aws"
	http_helper "github.com/gruntwork-io/terratest/modules/http-helper"
	"github.com/gruntwork-io/terratest/modules/terraform"
	//"github.com/stretchr/testify/assert"
	//"github.com/stretchr/testify/require"
	"fmt"
	//"strings"
	"testing"
	"time"
)

func TestTerraFormNetworking(t *testing.T) {
	//Run all tests in parallel mode
	//t.Parallel()

	//asigin test region
	awsRegion := "ca-central-1"

	//Declare Terraform .tfvars
	//terraFormVars := "testvar.tfvars"
	// Construct the terraform options with default retryable errors to handle the most common retryable errors in
	// terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		//path to terraform code
		TerraformDir: "../",
		// Variables stored in var.tfvars to pass to our Terraform code using -var-file options
		VarFiles: []string{"testvar.tfvars"},
	})

	// At the end of the test, run `terraform destroy` to clean up any resources that were created
	defer terraform.Destroy(t, terraformOptions)

	// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
	terraform.InitAndApply(t, terraformOptions)
	//=====Network test=====
	//Get VPC's id
	//vpcId := terraform.Output(t, terraformOptions, "vpc_id")
	//Get public subnet's id
	//publicSubnetId := terraform.Output(t, terraformOptions, "public_subnet_id")
	//subnets := aws.GetSubnetsForVpc(t, vpcId, awsRegion)
	//require.Equal(t, 1, len(subnets))

	// Verify if the network that is supposed to be public is really public
	//assert.True(t, aws.IsPublicSubnet(t, publicSubnetId, awsRegion))

	//======Instance test=====
	//Get instance's id
	//instanceId := terraform.Output(t, terraformOptions, "ec2_instance_id")
	//Get instance's public ip
	maxRetries := 10
	timeBetweenRetries := 5 * time.Second
	//instancePublicIp := aws.GetPublicIpOfEc2Instance(t, instanceId, awsRegion)
	instancePublicIp := terraform.Output(t, terraformOptions, "ec2_instance_public_ip")
	// Verify that we get back a 200 OK with the body "Hello, Tom!"
	url := fmt.Sprintf("http://%s:80", instancePublicIp)
	http_helper.HttpGetWithRetry(t, url, nil, 200, "Hello, Tom!", maxRetries, timeBetweenRetries)
}
