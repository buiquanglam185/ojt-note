resource "aws_instance" "terra_ec2" {
    ami = "${var.ami}"
    count = "${var.instance_count}"
    instance_type = "${var.instance_type}"
    key_name = "${var.key_name}"
    subnet_id = "${var.subnet_id}"
    vpc_security_group_ids = ["${var.sg_id}"]
    associate_public_ip_address = true
    user_data = <<EOF
#! /bin/bash
sudo apt-get update
sudo apt-get install -y apache2
sudo systemctl start apache2
sudo systemctl enable apache2
echo "Hello, Tom!" | sudo tee /var/www/html/index.html
EOF
    tags = {
        Name = "${var.env}-thanhhv18-ec2-${count.index + 1}"
        Environment = "${var.env}"
    }
}

resource "aws_key_pair" "terra_key_pair" {
    key_name = "${var.key_name}"
    public_key = "${file(var.public_key_path)}"
}