module "networking" {
    source = "./modules/networking"
    vpc_cidr = var.vpc_cidr
    subnet_name = var.subnet_name
    subnet_az = var.subnet_az
    subnet_cidr =var.subnet_cidr
    env = var.env
    vpc_name = var.vpc_name
    igw_name = var.igw_name
    route_table_name = var.route_table_name
    sg_name = var.sg_name
}

module "ec2" {
    source = "./modules/ec2"
    ami = var.ami
    instance_type = var.instance_type
    key_name = var.key_name
    subnet_id = module.networking.subnet_id
    env = var.env
    public_key_path = var.public_key_path
    sg_id = module.networking.sg_id
    instance_count = var.instance_count
    depends_on = [
      module.networking
    ]
}