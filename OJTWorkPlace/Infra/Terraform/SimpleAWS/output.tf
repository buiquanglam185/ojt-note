output "vpc_id" {
  value = module.networking.vpc_id
}
output "public_subnet_id" {
  value = module.networking.subnet_id
}
output "ec2_public_ip" {
  value = module.ec2.ec2_public_ip
}
output "ec2_instance_id" {
  value = module.ec2.ec2_instance_id
}
